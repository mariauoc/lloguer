package com.mycompany.lloguer.model;

import com.mycompany.lloguer.exceptions.LloguerException;

/**
 *
 * @author mfontana
 */
public class Coche extends Vehiculo {

    private int plazas;
    private int puertas;

    public Coche(int plazas, int puertas, String matricula) throws LloguerException {
        super(matricula, 20);
        this.plazas = plazas;
        this.puertas = puertas;
    }

    @Override
    public String toString() {
        return "Coche: " + super.toString() + " Plazas: " + plazas + " Puertas: " + puertas;
    }

    @Override
    public double calcularAlquier(int dias) {
        return getPrecioBase() + plazas * dias;
    }


}
