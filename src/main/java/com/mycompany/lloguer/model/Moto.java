package com.mycompany.lloguer.model;

import com.mycompany.lloguer.exceptions.LloguerException;

/**
 *
 * @author mfontana
 */
public class Moto extends Vehiculo {

    private int cc;

    public Moto(int cc, String matricula) throws LloguerException {
        super(matricula, 10);
        this.cc = cc;
    }

    @Override
    public double calcularAlquier(int dias) {
        return 5 * dias;
    }

}
