package com.mycompany.lloguer.model;

import com.mycompany.lloguer.exceptions.LloguerException;

/**
 *
 * @author mfontana
 */
public class Furgoneta extends Vehiculo {
    private int pma;

    public Furgoneta(int pma, String matricula) throws LloguerException {
        super(matricula, 30);
        this.pma = pma;
    }

    @Override
    public double calcularAlquier(int dias) {
        return 3 * dias;
    }
    
    
}
