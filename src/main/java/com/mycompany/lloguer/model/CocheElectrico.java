package com.mycompany.lloguer.model;

import com.mycompany.lloguer.exceptions.LloguerException;

/**
 *
 * @author mfontana
 */
public class CocheElectrico extends Coche {
    private int potencia;

    public CocheElectrico(int potencia, int plazas, int puertas, String matricula) throws LloguerException {
        super(plazas, puertas, matricula);
        this.potencia = potencia;
    }
    
    
    
}
