package com.mycompany.lloguer.model;

import com.mycompany.lloguer.exceptions.LloguerException;

/**
 *
 * @author mfontana
 */
public abstract class Vehiculo {
    private String matricula;
    private double precioBase;

    public Vehiculo(String matricula, double precioBase) throws LloguerException {
        if (matricula.length() != 7) {
            throw new LloguerException("Longitud de matrícula incorrecta");
        }
        this.matricula = matricula;
        this.precioBase = precioBase;
    }
    
    public abstract double calcularAlquier(int dias);


    @Override
    public String toString() {
        return matricula + " - precio base: " + precioBase + "€";
    }

    public double getPrecioBase() {
        return precioBase;
    }
    
}
