package com.mycompany.lloguer.model;

/**
 *
 * @author mfontana
 */
public interface Alquilable {
    
    public double calcularAlquiler(int dias);
    
}
