package com.mycompany.lloguer.exceptions;

/**
 *
 * @author mfontana
 */
public class LloguerException extends Exception {

    public LloguerException(String message) {
        super(message);
    }
    
}
