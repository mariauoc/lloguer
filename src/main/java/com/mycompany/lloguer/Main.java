package com.mycompany.lloguer;

import com.mycompany.lloguer.exceptions.LloguerException;
import com.mycompany.lloguer.model.Coche;
import com.mycompany.lloguer.model.CocheElectrico;
import com.mycompany.lloguer.model.Furgoneta;
import com.mycompany.lloguer.model.Moto;
import com.mycompany.lloguer.model.Vehiculo;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Vehiculo> flota = new ArrayList<>();
        try {
            // Añadimos un vehículo de cada para testear
            flota.add(new Coche(5, 5, "124ABC"));
            flota.add(new Moto(500, "888MMM"));
            flota.add(new Furgoneta(1000, "6543DDD"));
            flota.add(new CocheElectrico(800, 5, 3, "0123BCD"));
        } catch (LloguerException ex) {
            System.out.println(ex.getMessage());
        }

        for (Vehiculo v : flota) {
            System.out.println(v);
        }
    }

}
